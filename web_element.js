class InputText{
   constructor(driver, data){
      this.driver = driver;
      this.data = data;
   }
   
   async filterInputText(InputTextName){
      for(let i=0; i< this.data.length; i++){
         if((this.data[i].name == InputTextName)&&(this.data[i].type == "Input")){
            return this.data[i].path;
         }
      }
   }

   async waitEnabled(InputTextName){
      let filter = await this.filterElement(InputTextName)
      let element = await driver.findElement(filter);
      await driver.wait(until.elementIsEnabled(element), 5000, "Web element is disabled");
      return element;
   }
 
   async insertText(InputTextName, Value){
      let element = await this.waitVisible(InputTextName);
      await element.sendKeys(Value);
   }
}
