const {By} = require("selenium-webdriver");

const element_SideMenu = [
   //------------------------------------------------------------Button
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[1]/div/div/a[1]/div"),
      name: "System Overview",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[1]/div/div/a[2]/div"),
      name: "Plate",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[1]/div/div/a[3]/div"),
      name: "Imager",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[1]/div/div/a[4]/div"),
      name: "Schedule",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[1]/div/div/a[5]/div"),
      name: "Temperature",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[1]/div/div/a[6]/div"),
      name: "Plate Setup",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[1]/div/div/a[7]/div"),
      name: "Tools",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[1]/div/div/a[8]/div"),
      name: "Settings",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[1]/div/div/a[9]/div"),
      name: "Report and Supporrt",
      type: "Button"
   }
]

module.exports = element_SideMenu;