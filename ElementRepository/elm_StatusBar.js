const {By} = require("selenium-webdriver");

const StatusBar = [
   //--------------------------------------------------------Button
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[1]/div"),
      name: "System",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[2]/div"),
      name: "Imager",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[3]/div"),
      name: "PSR",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[4]/div"),
      name: "Load Port",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[5]/div"),
      name: "Temperature",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[6]/div"),
      name: "Schedule",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[7]/div"),
      name: "Image Processor",
      type: "Button"
   },
   //--------------------------------------------------------Text
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[1]/div[2]/div/div/div[2]"),
      name: "System",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div/div/div[2]"),
      name: "Imager",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[3]/div[2]/div/div[1]/div[2]"),
      name: "PSR",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[3]/div[2]/div/div[2]/div/div/span[1]"),
      name: "PSR X",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[3]/div[2]/div/div[2]/div/div/span[2]"),
      name: "PSR Y",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[3]/div[2]/div/div[2]/div/div/span[3]"),
      name: "PSR Z",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[4]/div[2]/div/div/div[2]"),
      name: "Load Port",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[5]/div[2]/div/div/div[2]"),
      name: "Temperature",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[6]/div[2]/div/div/div[2]"),
      name: "Schedule",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[7]/div[2]/div/div/div[2]"),
      name: "Image Processor",
      type: "Text"
   },
   //--------------------------------------------------------Layer
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[1]/div[2]"),
      name: "System",
      type: "Layer"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[2]/div[2]"),
      name: "Imager",
      type: "Layer"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[3]/div[2]"),
      name: "PSR",
      type: "Layer"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[4]/div[2]"),
      name: "Load Port",
      type: "Layer"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[5]/div[2]"),
      name: "Temperature",
      type: "Layer"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[6]/div[2]"),
      name: "Schedule",
      type: "Layer"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[3]/div/div[7]/div[2]"),
      name: "Image Processor",
      type: "Layer"
   }
]

module.exports = StatusBar;