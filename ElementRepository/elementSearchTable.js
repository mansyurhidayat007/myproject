const {By} = require("selenium-webdriver");

const SearchTable = [
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[2]/div/div[1]/table/thead/tr/th[7]/div/div[2]/input"),
      name: "Search - Barcode",
      type: "Input"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[2]/div/div[1]/table/thead/tr/th[8]/div/div[2]/input"),
      name: "Search - Location",
      type: "Input"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[2]/div/div[1]/table/tbody/tr/td[8]"),
      name: "Search - Location 1",
      type: "Text"
   }
]

module.exports = SearchTable;