const {By} = require("selenium-webdriver");

const MainMenu = [
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[1]/div/div/a[2]/div"),
      name: "Plate",
      type: "Button"
   },
   {
      path: By.xpath("//div[@class='sidebar-item']//div[text()='Imager']"),
      name: "Imager",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[2]/div/a[2]/div"),
      name: "Search",
      type: "Button"
   },
]

module.exports = MainMenu;