const {By} = require("selenium-webdriver");

const SubMenu = [
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[2]/div/a[1]/div"),
      name: "Imager - Live Image",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div/a[1]/div"),
      name: "Plate - Load Port",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[2]/div/a[2]/div"),
      name: "Plate - Search",
      type: "Button"
   }
]

module.exports = SubMenu;