const {By} = require("selenium-webdriver");

const LiveImageHeader = [
   {
      path: By.id("MOVE_TO"),
      name: "Move To",
      type: "Button"
   },
   {
      path: By.id("CLEAR_DROP_LOCATION"),
      name: "Clear Drop",
      type: "Button"
   },
   {
      path: By.id("CANCEL_IMAGING"),
      name: "Cancel Imaging",
      type: "Button"
   },
   {
      path: By.id("IMAGE_PLATE"),
      name: "Image Now",
      type: "Button"
   },
   {
      path: By.id("CAPTURE_EFI"),
      name: "Image Now",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[1]/div/div[2]/div[2]/div/div[2]"),
      name: "Storage",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[1]/div/div[2]/div[2]/div/div[3]"),
      name: "Load Port",
      type: "Button"
   }
]

module.exports = LiveImageHeader;