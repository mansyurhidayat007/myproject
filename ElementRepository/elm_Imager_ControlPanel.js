const {By} = require("selenium-webdriver");

const element_ImagerControlPanel = [
   //------------------------------------------------------------Button
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[1]/div[2]"),
      name: "Control Panel",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[2]/div[2]"),
      name: "Well Control",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[3]/div[2]"),
      name: "Optics and Camera Control",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[4]/div[2]"),
      name: "Leveling",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[5]/div[2]"),
      name: "Live Image Options",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[6]/div[2]"),
      name: "Plate Information",
      type: "Button"
   },
   //------------------------------------------------------------Graphic
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[5]/div[2]/svg"),
      name: "Control Panel",
      type: "Graphic"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[2]/div[2]/svg"),
      name: "Well Control",
      type: "Graphic"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[3]/div[2]/svg"),
      name: "Optics and Camera Control",
      type: "Graphic"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[4]/div[2]/svg"),
      name: "Leveling",
      type: "Graphic"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[5]/div[2]/svg"),
      name: "Live Image Options",
      type: "Graphic"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[6]/div[2]/svg"),
      name: "Plate Information",
      type: "Graphic"
   },
   //------------------------------------------------------------Text
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[1]/div[1]/div"),
      name: "Control Panel",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[2]/div[1]/div/div[1]"),
      name: "Well Control",
      type: "Text"
   },
   {
      path: By.xpath("//*[@id='AcccordionToogleChildren']//div[text()='Optics and Camera Control']"),
      name: "Optics and Camera Control",
      type: "Text"
   },
   {
      path: By.xpath("//*[@id='AcccordionToogleChildren']//div[text()='Leveling']"),
      name: "Leveling",
      type: "Text"
   },
   {
      path: By.xpath("//*[@id='AcccordionToogleChildren']//div[text()='Live Image Options']"),
      name: "Live Image Options",
      type: "Text"
   },
   {
      path: By.xpath("//*[@id='AcccordionToogleChildren']//div[text()='Plate Information']"),
      name: "Plate Information",
      type: "Text"
   },
   //------------------------------------------------------------Layer
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[7]"),
      name: "Plate Information",
      type: "Layer"
   }
]

module.exports = element_ImagerControlPanel;