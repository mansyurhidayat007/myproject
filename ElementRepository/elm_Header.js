const {By} = require("selenium-webdriver");

const Header = [
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[1]/div/div/div/svg/path"),
      name: "Rock Imager Logo",
      type: "Graphic"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[1]/div/div[1]/span[1]"),
      name: "Rock Imager Title",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[1]/div/div[1]/div/a"),
      name: "Navigation Level 1",
      type: "Text"
   }
]

module.exports = Header;