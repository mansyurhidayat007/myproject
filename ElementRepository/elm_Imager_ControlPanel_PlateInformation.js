const {By} = require("selenium-webdriver");

const PlateInformation = [
   {
      path: By.xpath("//th[text()='Project:']/following-sibling::td"),
      name: "Project",
      type: "Text"
   },
   {
      path: By.xpath("//th[text()='Experiment:']/following-sibling::td"),
      name: "Experiment",
      type: "Text"
   },
   {
      path: By.xpath("//th[text()='Plate Type:']/following-sibling::td"),
      name: "Plate Type",
      type: "Text"
   },
   {
      path: By.xpath("//th[text()='Location:']/following-sibling::td"),
      name: "Location",
      type: "Text"
   },
   {
      path: By.xpath("//th[text()='Barcode:']/following-sibling::td"),
      name: "Barcode",
      type: "Text"
   },
   {
      path: By.xpath("//th[text()='Plate Id:']/following-sibling::td"),
      name: "Plate Id",
      type: "Text"
   },
   {
      path: By.xpath("//th[text()='User Name:']/following-sibling::td"),
      name: "User Name",
      type: "Text"
   },
]

module.exports = PlateInformation;