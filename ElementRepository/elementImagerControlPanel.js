const {By} = require("selenium-webdriver");

const ImagerControlPanel = [
   {
      path: By.css(".accordion-toggle:nth-child(6) svg"),
      name: "Plate Information",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[6]/div[2]/svg"),
      name: "Live Image Options",
      type: "Button"
   }
]

module.exports = ImagerControlPanel;