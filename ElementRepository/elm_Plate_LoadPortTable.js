const {By} = require("selenium-webdriver");

const LoadPortTable = [
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[1]/td[1]"),
      name: "Slot 1",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[2]/td[1]"),
      name: "Slot 2",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[3]/td[1]"),
      name: "Slot 3",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[4]/td[1]"),
      name: "Slot 4",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[5]/td[1]"),
      name: "Slot 5",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[6]/td[1]"),
      name: "Slot 6",
      type: "Button"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[7]/td[1]"),
      name: "Slot 7",
      type: "Button"
   },
   //-----------------------------------------------Status Result
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[1]/td[9]"),
      name: "Status 1",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[2]/td[9]"),
      name: "Status 2",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[3]/td[9]"),
      name: "Status 3",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[4]/td[9]"),
      name: "Status 4",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[5]/td[9]"),
      name: "Status 5",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[6]/td[9]"),
      name: "Status 6",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[7]/td[9]"),
      name: "Status 7",
      type: "Text"
   },
   //-----------------------------------------------Barcode
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[1]/td[7]"),
      name: "Barcode 1",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[2]/td[7]"),
      name: "Barcode 2",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[3]/td[7]"),
      name: "Barcode 3",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[4]/td[7]"),
      name: "Barcode 4",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[5]/td[7]"),
      name: "Barcode 5",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[6]/td[7]"),
      name: "Barcode 6",
      type: "Text"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[7]/td[7]"),
      name: "Barcode 7",
      type: "Text"
   },
   //-----------------------------------------------Check Box Layer
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[1]"),
      name: "Slot 1",
      type: "Layer"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[2]"),
      name: "Slot 2",
      type: "Layer"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[3]"),
      name: "Slot 3",
      type: "Layer"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[4]"),
      name: "Slot 4",
      type: "Layer"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[5]"),
      name: "Slot 5",
      type: "Layer"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[6]"),
      name: "Slot 6",
      type: "Layer"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[3]/div[1]/div/table/tbody/tr[7]"),
      name: "Slot 7",
      type: "Layer"
   },
]

module.exports = LoadPortTable;