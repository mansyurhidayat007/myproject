const {By} = require("selenium-webdriver");

const Plate_ControlPanel = [
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[2]"),
      name: "Control Panel Layer",
      type: "Layer"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[2]/div[2]/span"),
      name: "Sub Page Title",
      type: "Text"
   },
   {
      path: By.id("IMAGEPLATE"),
      name: "Image Now",
      type: "Button"
   },
   {
      path: By.id("MOVE"),
      name: "Move Plate To",
      type: "Button"
   },
   {
      path: By.id("IMAGER_Vis-UV Imager"),
      name: "Vis-UV Imager",
      type: "Button"
   },
   {
      path: By.id("STORAGE"),
      name: "Storage",
      type: "Button"
   },
   {
      path: By.id("LOADPORT"),
      name: "Load Port",
      type: "Button"
   },
   {
      path: By.id("INFO"),
      name: "Move Plate To",
      type: "Button"
   },
   {
      path: By.id("HANDSCAN"),
      name: "Hand Scan",
      type: "Button"
   },
   {
      path: By.id("barcodeInput"),
      name: "Hand Scan Input",
      type: "Input"
   },
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[4]/div/div[3]/div[2]/button"),
      name: "Hand Scan Ok",
      type: "Button"
   },
   {
      path: By.id("CLEARDROP"),
      name: "Clear All Drop Locations",
      type: "Button"
   },
]

module.exports = Plate_ControlPanel;