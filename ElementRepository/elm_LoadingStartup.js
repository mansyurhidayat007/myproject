const {By} = require("selenium-webdriver");

const LoadingStartup = [
   {
      path: By.xpath("/html/body/div/div/header/div"),
      name: "Loading Animation",
      type: "Graphic"
   },
   {
      path: By.xpath("/html/body/div/div/header/p"),
      name: "Loading Status",
      type: "Text"
   }
]

module.exports = LoadingStartup;