const {By} = require("selenium-webdriver");

const LoadPortHeader = [
   {
      path: By.xpath("/html/body/div/div/div[1]/div/div[2]/div/div[1]/div/div[2]/button[2]/div[2]"),
      name: "Scan Load Port",
      type: "Button"
   }
]

module.exports = LoadPortHeader;