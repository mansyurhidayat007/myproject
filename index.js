//Kill current running firefox browser
let cp = require("child_process");
cp.exec("kill_firefox.bat");

//Selenium Driver
const {Builder, By, Key, until} = require("selenium-webdriver");
let driver = new Builder().forBrowser("firefox").build();

//Include class file
const class_ImagerAction = require("./Class_Scenario/class_ImagerAction")
const class_Plate_LoadPortAction = require("./Class_Scenario/class_Plate_LoadPortAction")
const class_LoadingStartup = require("./Class_WebComponent/class_LoadingStartup")

async function openApp(){
   await driver.manage().window().maximize();
   await driver.get("http://10.200.2.232:5000/");
}

async function login(){
   //Wait until loading animation dissapear
   let loadingAnimation = await driver.findElement(By.xpath("//div[@class='loading-container']"));
   await driver.wait(until.stalenessOf(loadingAnimation), 100000);

   let inputtextUsername = await driver.findElement(By.name("username"));
   await driver.wait(until.elementIsVisible(inputtextUsername)); 
   await inputtextUsername.sendKeys("Administrator");

   //Input password
   let inputtextPassword = await driver.findElement(By.name("password"));
   await driver.wait(until.elementIsVisible(inputtextPassword));
   await inputtextPassword.sendKeys("fmlx216");

   //Click login button
   let buttonLogin = await driver.findElement(By.xpath("/html/body/div/div/div/div[1]/div[3]/button"));
   await driver.wait(until.elementIsVisible(buttonLogin));
   await buttonLogin.click();
}

async function Main(){
   let obj_LoadingStartup = new class_LoadingStartup(driver);
   let obj_ImagerAction = new class_ImagerAction(driver);
   let obj_Plate_LoadPortAction = new class_Plate_LoadPortAction(driver);
   
   await openApp();
   await login();

   try{
      await obj_LoadingStartup.WaitUntilFinish("Loading Animation", 10000);
      console.log("Loading Animation is Present");
   }
   catch{
      console.log("Loading Animation is Skipped");
   }

   await obj_Plate_LoadPortAction.MovePlateToImager(1);
   // await obj_ImagerAction.MoveToStorage(10000);
}

Main();
