const Button = require("../Class_BasicElement/class_Button");
const Data = require("../ElementRepository/elm_SideMenu")

//Include class file
class SideMenu{
   constructor(driver){
      this.driver = driver;
      this.data = Data;
      this.objButton = new Button(this.driver, this.data);
   }

   async OpenPage(value){
      await this.objButton.waitEnabled(value, 5000);
      await this.objButton.click(value);
   }
}

module.exports = SideMenu;