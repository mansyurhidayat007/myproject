const Text = require("../Class_BasicElement/class_Text");
const Button = require("../Class_BasicElement/class_Button");
const Graphic = require("../Class_BasicElement/class_Graphic");
const Layer = require("../Class_BasicElement/class_Layer");
const Data = require("../ElementRepository/elm_Imager_ControlPanel")

//Include class file
class ImagerControlPanel{
   constructor(driver){
      this.driver = driver;
      this.data = Data;
      this.objText = new Text(this.driver, this.data);
      this.objButton = new Button(this.driver, this.data);
      this.objGraphic = new Graphic(this.driver, this.data);
      this.objLayer = new Layer(this.driver, this.data);
   }

   async GetTitle(value){
      let textResult = await this.objText.getText(value);
      return textResult;
   }
   
   async IsPanelDisplayed(value){
      try{
         await this.objLayer.waitVisible(value);
      }
      catch{
         return false;
      }
      return true;
   }

   async OpenPanel(value){
      let PanelStatus = await this.IsPanelDisplayed();
      if(!PanelStatus){
         await this.objButton.click(value);
      }
      else{
         console.log(value + " panel is already open");
      }
   }

   async ClosePanel(value){
      let PanelStatus = await this.IsPanelDisplayed();
      if(PanelStatus){
         await this.objButton.click(value);
      }
      else{
         console.log(value + " panel is already close");
      }
   }
}

module.exports = ImagerControlPanel;