const Graphic = require("../Class_BasicElement/class_Graphic");
const Text = require("../Class_BasicElement/class_Text");
const Data = require("../ElementRepository/elm_LoadingStartup")

class StatusBar{
   constructor(driver){
      this.driver = driver;
      this.data = Data;
      this.objGraphic = new Graphic(this.driver, this.data);
      this.objText = new Text(this.driver, this.data);
   }

   async GetStatus(value){
      await this.objText.getText(value);
   }

   async WaitUntilFinish(value, time){
      await this.objGraphic.waitStalenessOf(value, time);
   }
}

module.exports = StatusBar;