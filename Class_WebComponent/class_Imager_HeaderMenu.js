const Button = require("../Class_BasicElement/class_Button");
const Data = require("../ElementRepository/elm_Imager_HeaderMenu")

//Include class file
class Imager_HeaderMenu{
   constructor(driver){
      this.driver = driver;
      this.data = Data;
      this.objButton = new Button(this.driver, this.data);
   }

   async Execute(value){
      await this.objButton.click(value);
   }
}

module.exports = Imager_HeaderMenu;