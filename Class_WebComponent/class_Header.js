const Graphic = require("../Class_BasicElement/class_Graphic");
const Text = require("../Class_BasicElement/class_Text");
const Data = require("../ElementRepository/elm_Header")

class Header{
   constructor(driver){
      this.driver = driver;
      this.data = Data;
      this.objGraphic = new Graphic(this.driver, this.data);
      this.objText = new Text(this.driver, this.data);
   }

   async WaitUntilVisible(value, time){
      await this.objGraphic.waitVisible(value, time);
   }

   async WaitUntilLocated(value, time){
      await this.objGraphic.waitLocated(value, time);
   }

   async GetTitle(value){
      await this.objText.getText(value);
   }

   async WaitTitleChangeTo(value, text, time){
      await this.objText.waitTextIs(value, text, time);

   }

   async WaitTitleContains(value, text, time){
      await this.objText(value, text, time);
   }
}

module.exports = Header;