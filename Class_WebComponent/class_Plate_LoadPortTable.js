const Button = require("../Class_BasicElement/class_Button");
const Input = require("../Class_BasicElement/class_InputText");
const Text = require("../Class_BasicElement/class_Text");
const Layer = require("../Class_BasicElement/class_Layer");
const Data = require("../ElementRepository/elm_Plate_LoadPortTable")

//Include class file
class Plate_LoadPortTable{
   constructor(driver){
      this.driver = driver;
      this.data = Data;
      this.objButton = new Button(this.driver, this.data);
      this.objInput = new Input(this.driver, this.data);
      this.objText = new Text(this.driver, this.data);
      this.objLayer = new Layer(this.driver, this.data);
   }

   async Input(value){
      await this.objInput.insertValue(value);
   }

   async GetBarcode(slotNumber){
      let textResult = await this.objText.getText("Barcode " + slotNumber);
      return textResult;
   }

   async IsSlotSelected(slotNumber){
      let layerAttribute = await this.objLayer.getAttribute("Slot " + slotNumber, "style")
      if(layerAttribute == ""){
         return false;
      }
      else if(layerAttribute !== ""){
         return true;
      }
   }

   async SelectSlot(slotNumber){
      let IsSlotSelectedResult = await this.IsSlotSelected(slotNumber);
      if(!IsSlotSelectedResult){
         await this.objButton.click("Slot " + slotNumber);
      }
      else{
         console.log("Slot " +slotNumber+ " is already selected");
      }
   }

   async UnselectSlot(slotNumber){
      let IsSlotSelectedResult = await this.IsSlotSelected();
      if(!IsSlotSelectedResult){
         await this.objButton.click("Slot " + slotNumber);
      }
      else{
         console.log("Slot " +slotNumber+ " is already unselected");
      }
   }
}

module.exports = Plate_LoadPortTable;