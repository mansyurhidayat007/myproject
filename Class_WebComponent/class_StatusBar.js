const Button = require("../Class_BasicElement/class_Button");
const Text = require("../Class_BasicElement/class_Text");
const Layer = require("../Class_BasicElement/class_Layer");
const Data = require("../ElementRepository/elm_StatusBar")

class StatusBar{
   constructor(driver){
      this.driver = driver;
      this.data = Data;
      this.objButton = new Button(this.driver, this.data);
      this.objLayer = new Layer(this.driver, this.data);
      this.objText = new Text(this.driver, this.data);
   }

   async IsPanelDisplayed(value){
      try{
         await this.objLayer.waitVisible(value);
      }
      catch{
         console.log("Panel " +value+ " is not displayed")
         return false;
      }
      return true;
   }

   async Open(value){
      let PanelStatus = await this.IsPanelDisplayed(value);
      if(!PanelStatus){
         await this.objButton.click(value);
      }
      else{
         console.log(value + " is already open");
      }
   }

   async Close(value){
      let PanelStatus = await this.IsPanelDisplayed(value);
      if(PanelStatus){
         await this.objButton.click(value);
      }
      else{
         console.log(value + " is already close");
      }
   }

   async GetStatus(value){
      let PanelStatus = await this.IsPanelDisplayed(value);
      if(!PanelStatus){
         await this.Open(value);
      }
      let StatusResult = await this.objText.getText(value);
      return StatusResult;
   }

   async WaitStatusChangeTo(value, text, time){
      let PanelStatus = await this.IsPanelDisplayed(value);
      if(!PanelStatus){
         await this.Open(value);
      }
      await this.objText.waitTextIs(value, text, time);
   }

   async WaitStatusContains(value, text, time){
      let PanelStatus = await this.IsPanelDisplayed(value);
      if(!PanelStatus){
         await this.Open(value);
      }
      await this.objText.waitTextContains(value, text, time);
   }
}

module.exports = StatusBar;