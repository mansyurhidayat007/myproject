const Text = require("../Class_BasicElement/class_Text");
const Data = require("../ElementRepository/elm_Imager_ControlPanel_PlateInformation")

//Include class file
class PlateInformation{
   constructor(driver){
      this.driver = driver;
      this.data = Data;
      this.objText = new Text(this.driver, this.data);
   }

   async GetText(value){
      let textResult = await this.objText.getText(value);
      return textResult;
   }
   
   async WaitTextIs(value, text, time){
      await this.objText.waitTextIs(value, text, time);
   }

   async WaitTextContains(value, text, time){
      await this.objText.waitTextContains(value, text, time);
   }
}

module.exports = PlateInformation;