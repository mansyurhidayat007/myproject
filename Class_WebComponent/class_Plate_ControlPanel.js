const Text = require("../Class_BasicElement/class_Text");
const Button = require("../Class_BasicElement/class_Button");
const Graphic = require("../Class_BasicElement/class_Graphic");
const Layer = require("../Class_BasicElement/class_Layer");
const Data = require("../ElementRepository/elm_Plate_ControlPanel")

//Include class file
class Plate_ControlPanel{
   constructor(driver){
      this.driver = driver;
      this.data = Data;
      this.objText = new Text(this.driver, this.data);
      this.objButton = new Button(this.driver, this.data);
      this.objGraphic = new Graphic(this.driver, this.data);
      this.objLayer = new Layer(this.driver, this.data);
   }

   async IsPanelDisplayed(value){
      try{
         await this.objLayer.waitVisible(value, 5000);
      }
      catch{
         return false;
      }
      return true;
   }

   async IsButtonEnabled(value){
      let ControlPanelLayer = await this.IsPanelDisplayed("Control Panel Layer");
      if(ControlPanelLayer){
         try{
            await this.objButton.waitEnabled(value, 5000);
            return true;
         }
         catch{
            return false;
         }
      }
      else{
         console.log("Page " +value+ " is not displayed");
      }
   }

   async ExecuteMoveToStorage(){
      let btnMovePlateToStatus = await this.IsButtonEnabled("Move Plate To");
      if(btnMovePlateToStatus){
         await this.objButton.click("Move Plate To");
         let SubPageTitle = await this.objText.getText("Sub Page Title");
         if(SubPageTitle == "Move Plate"){
            let btnStorageStatus = await this.IsButtonEnabled("Storage");
            if(btnStorageStatus){
               await this.objButton.click("Storage");
            }
            else{
               console.log("Button 'Storage' is disabled");
            }
         }
         else{
            console.log("Sub page 'Move Plate' is not displayed");
         }
      }
      else{
         console.log("Button 'Move Plate To' is disabled");
      }
   }

   async ExecuteMoveToImager(){
      let btnMovePlateToStatus = await this.IsButtonEnabled("Move Plate To");
      if(btnMovePlateToStatus){
         await this.objButton.click("Move Plate To");
         let SubPageTitle = await this.objText.getText("Sub Page Title");
         if(SubPageTitle == "Move Plate"){
            let btnStorageStatus = await this.IsButtonEnabled("Vis-UV Imager");
            if(btnStorageStatus){
               await this.objButton.click("Vis-UV Imager");
            }
            else{
               console.log("Button 'Storage' is disabled");
            }
         }
         else{
            console.log("Sub page 'Move Plate' is not displayed");
         }
      }
      else{
         console.log("Button 'Move Plate To' is disabled");
      }
   }

   async ExecuteHandScan(){
   }
}

module.exports = Plate_ControlPanel;