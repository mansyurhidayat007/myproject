//Include class file
const class_SideMenu = require("../Class_WebComponent/class_SideMenu");
const class_Plate_LoadPortTable = require("../Class_WebComponent/class_Plate_LoadPortTable")
const class_Plate_ControlPanel = require("../Class_WebComponent/class_Plate_ControlPanel")

class Plate_LoadPortAction{
   constructor(driver){
      this.driver = driver;
      this.obj_SideMenu = new class_SideMenu(this.driver);
      this.obj_Plate_LoadPortTable = new class_Plate_LoadPortTable(this.driver);
      this.obj_Plate_ControlPanel = new class_Plate_ControlPanel(this.driver);
   }

   async IsSlotEmpty(slotNumber){
      await this.obj_SideMenu.OpenPage("Plate");
      let Barcode = await this.obj_Plate_LoadPortTable.GetBarcode(slotNumber);

      if(Barcode != ""){
         return false;
      }
      else{
         return true; 
      }
   }

   async ScanLoadPort(){

   }

   async HandScan(){

   }

   async StoreContent(){

   }

   async StoreActiveContent(){

   }

   async MovePlateToImager(slotNumber){
      let IsSlotEmptyResult = await this.IsSlotEmpty(slotNumber);
      if(!IsSlotEmptyResult){
         await this.obj_Plate_LoadPortTable.SelectSlot(slotNumber);
         await this.obj_Plate_ControlPanel.ExecuteMoveToImager();
      }
      else{
         console.log("Load Port - Slot " +slotNumber+ " is empty");
      }
   }

   async MovePlateToStorage(){
      
   }
}

module.exports = Plate_LoadPortAction;