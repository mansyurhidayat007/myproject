//Include class file
const class_Imager_ControlPanel = require("../Class_WebComponent/class_Imager_ControlPanel");
const class_StatusBar = require("../Class_WebComponent/class_StatusBar");
const class_SideMenu = require("../Class_WebComponent/class_SideMenu");
const class_PlateInformation = require("../Class_WebComponent/class_Imager_ControlPanel_PlateInformation");
const class_Imager_HeaderMenu = require("../Class_WebComponent/class_Imager_HeaderMenu");

class ImagerAction{
   constructor(driver){
      this.driver = driver;
      this.obj_SideMenu = new class_SideMenu(this.driver);
      this.obj_Imager_ControlPanel = new class_Imager_ControlPanel(this.driver);
      this.obj_PlateInformation = new class_PlateInformation(this.driver);
      this.obj_Imager_HeaderMenu = new class_Imager_HeaderMenu(this.driver);
      this.obj_StatusBar = new class_StatusBar(this.driver);
   }

   async GetPlateBarcode(){
      await this.obj_SideMenu.OpenPage("Imager");
      await this.obj_Imager_ControlPanel.OpenPanel("Plate Information");

      let Barcode = await this.obj_PlateInformation.GetText("Barcode");
      if(Barcode == "No plate"){
         console.log("Currently, there is no plate available on the imager");
         return "No plate";
      }
      else{
         return Barcode;
      }
   }

   async MoveToLoadPort(timeWait){
      let PlateBarcode = await this.GetPlateBarcode();
      if(PlateBarcode != "No plate"){
         await this.obj_Imager_HeaderMenu.Execute("Move To");
         await this.obj_Imager_HeaderMenu.Execute("Load Port");
         try{
            await this.obj_StatusBar.WaitStatusChangeTo("PSR", "Returned", timeWait);
         }
         catch{
            console.log("[Failed] - PSR Status doesn't change to Returned");
            return false;
         }
      }
   }

   async MoveToStorage(timeWait){
      let PlateBarcode = await this.GetPlateBarcode();
      if(PlateBarcode!= "No plate"){
         await this.obj_Imager_HeaderMenu.Execute("Move To");
         await this.obj_Imager_HeaderMenu.Execute("Storage");
         try{
            await this.obj_StatusBar.WaitStatusChangeTo("PSR", "Returned", timeWait);
         }
         catch{
            console.log("[Failed] - PSR Status doesn't change to Returned");
            return false;
         }
      }
   }
}

module.exports = ImagerAction;