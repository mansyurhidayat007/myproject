const {until} = require("selenium-webdriver");

class InputText{
   constructor(driver, data){
      this.driver = driver;
      this.data = data;
   }
   
   async filterText(textName){
      for(let i=0; i< this.data.length; i++){
         if(this.data[i].type == "Input"){
            if(this.data[i].name == textName){
               return this.data[i].path;
            }
         }
      }
   }

   async waitEnabled(textName, waitingTime){
      let filter = await this.filterText(textName);
      let element = await this.driver.findElement(filter);
      await this.driver.wait(until.elementIsEnabled(element), waitingTime, textName + " failed to enable");
   }

   async waitDisabled(textName, waitingTime){
      let filter = await this.filterText(textName);
      let element = await this.driver.findElement(filter);
      await this.driver.wait(until.elementIsDisabled(element), waitingTime, textName + " failed to disable");
   }
 
   async insertValue(textName, value){
      let filter = await this.filterText(textName);
      let element = await this.driver.findElement(filter);
      await element.sendKeys(value);
   }
}

module.exports = InputText;