const {until} = require("selenium-webdriver");

class Text{
   constructor(driver, data){
      this.driver = driver;
      this.data = data;
   }
   
   async filterText(textName){
      for(let i=0; i< this.data.length; i++){
         if(this.data[i].type == "Text"){
            if(this.data[i].name == textName){
               return this.data[i].path;
            }
         }
      }
   }

   async waitTextIs(textName, text, waitingTime){
      let filter = await this.filterText(textName)
      let element = await this.driver.findElement(filter);
      await this.driver.wait(until.elementTextIs(element, text), waitingTime, "Text failed changes to : " + text);
   }

   async waitTextContains(textName, text, waitingTime){
      let filter = await this.filterText(textName)
      let element = await this.driver.findElement(filter);
      await this.driver.wait(until.elementTextContains(element, text), waitingTime, "Text is not contain : " + text);
   }

   async getText(textName){
      let filter = await this.filterText(textName)
      let element = await this.driver.findElement(filter);
      let textResult = await element.getText();
      return textResult;
   }
}

module.exports = Text;