const {until} = require("selenium-webdriver");

class Graphic{
   constructor(driver, data){
      this.driver = driver;
      this.data = data;
   }
   
   async filterContent(value){
      for(let i=0; i< this.data.length; i++){
         if(this.data[i].type == "Graphic"){
            if(this.data[i].name == value){
               return this.data[i].path;
            }
         }
      }
   }

   async waitVisible(value, waitingTime){
      let filter = await this.filterContent(value);
      let element = await this.driver.findElement(filter);
      await this.driver.wait(until.elementIsVisible(element), waitingTime, "Graphic element is failed to visible");
   }

   async waitLocated(value, waitingTime){
      let filter = await this.filterContent(value);
      let element = await this.driver.findElement(filter);
      await this.driver.wait(until.elementLocated(element), waitingTime, "Graphic element is failed to locate");
   }

   async waitNotVisible(value, waitingTime){
      let filter = await this.filterContent(value);
      let element = await this.driver.findElement(filter);
      await this.driver.wait(until.elementIsNotVisible(element), waitingTime, "Graphic element is failed to not visible");
   }

   async waitStalenessOf(value, waitingTime){
      let filter = await this.filterContent(value);
      let element = await this.driver.findElement(filter);
      await this.driver.wait(until.stalenessOf(element), waitingTime, "Graphic element is failed to stale");
   }

   async getAttribute(value, AttributeType){
      let filter = await this.filterContent(value);
      let element = await this.driver.findElement(filter);
      let Attribute = await element.getAttribute(AttributeType);
      return Attribute;
   }
}

module.exports = Graphic;