const {until} = require("selenium-webdriver");

class Button{
   constructor(driver, data){
      this.driver = driver;
      this.data = data;
   }
   
   async filterButton(buttonName){
      for(let i=0; i< this.data.length; i++){
         if(this.data[i].type == "Button"){
            if(this.data[i].name == buttonName){
               return this.data[i].path;
            }
         }
      }
   }

   async waitLocated(buttonName, waitingTime){
      let filter = await this.filterButton(buttonName);
      let element = await this.driver.findElement(filter);
      await this.driver.wait(until.elementLocated(element), waitingTime, "Failed to locate button");
   }

   async waitEnabled(buttonName, waitingTime){
      let filter = await this.filterButton(buttonName);
      let element = await this.driver.findElement(filter);
      try{
         await this.driver.wait(until.elementIsEnabled(element), waitingTime);
         return element;
      }
      catch{
         console.log("Failed to enable button : " +buttonName);
      }
   }

   async waitDisabled(buttonName, waitingTime){
      let filter = await this.filterButton(buttonName);
      let element = await this.driver.findElement(filter);
      try{
         await this.driver.wait(until.elementIsDisabled(element), waitingTime);
      }
      catch{
         console.log("Failed to disable button : " +buttonName);
      }
   }

   async click(buttonName){
      //let filter = await this.filterButton(buttonName);
      //let element = await this.driver.findElement(filter);
      let element = await this.waitEnabled(buttonName, 5000);
      await element.click();
   }
}

module.exports = Button;